package com.rapidtech.springdasar.singleton;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DatabaseTest {
    @Test
    void singletonTest() {
        Database database1 = Database.getInstance();
        Database database2 = Database.getInstance();
        Database database3 = Database.getInstance();

        Database database4 = Database.getInstance();
        Database database5 = Database.getInstance();
        Database database6 = Database.getInstance();
        Database database7 = Database.getInstance();

        Assertions.assertSame(database1, database2);
        Assertions.assertSame(database1, database3);

        Assertions.assertSame(database1, database4);
        Assertions.assertSame(database1, database5);
        Assertions.assertSame(database1, database6);
        Assertions.assertSame(database1, database7);
    }
}
