package com.rapidtech.springdasar.config;

import com.rapidtech.springdasar.model.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ProductConfigurationTest {
    private ApplicationContext context;
    @BeforeEach
    void setUp(){
        context = new AnnotationConfigApplicationContext(ProductConfiguration.class);
    }

    @Test
    void productCatTest(){
        Category category = context.getBean(Category.class);
        Product product = context.getBean(Product.class);
        ProductCategory productCategory = context.getBean(ProductCategory.class);

        Assertions.assertSame(category, productCategory.getCategory());
        Assertions.assertSame(product, productCategory.getProduct());
    }

}
