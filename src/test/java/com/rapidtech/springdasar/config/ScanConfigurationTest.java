package com.rapidtech.springdasar.config;

import com.rapidtech.springdasar.model.Bar;
import com.rapidtech.springdasar.model.Foo;
import com.rapidtech.springdasar.model.ProductService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ScanConfigurationTest {
    private ApplicationContext context;
    @BeforeEach
    void setUp(){
        context = new AnnotationConfigApplicationContext(ScanConfiguration.class);

    }

    @Test
    void scanTest(){
        ProductService productService = context.getBean(ProductService.class);

        Assertions.assertNotNull(productService);
    }
}
