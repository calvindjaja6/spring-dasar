package com.rapidtech.springdasar.config;

import com.rapidtech.springdasar.model.Bar;
import com.rapidtech.springdasar.model.Foo;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BarConfiguration {
    @Bean
    public Bar bar(){
        return new Bar();
    }
}
