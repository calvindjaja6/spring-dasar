package com.rapidtech.springdasar.config;

import com.rapidtech.springdasar.model.Product;
import com.rapidtech.springdasar.model.ProductService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {
        "programmerzamannow.spring.core.configuration"
})
public class ScanConfiguration {
    @Bean
    public ProductService productService(){
        return new ProductService();
    }
}
